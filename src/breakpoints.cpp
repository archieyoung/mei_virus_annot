#include "breakpoints.h"

Breakpoint::Breakpoint(std::string _ref_name_1, uint32_t _ref_pos1,
        std::string _ref_name_2, uint32_t _ref_pos2, int _length,
        Evidence_type _evidence_type, std::string _query_name,
        uint32_t _query_start, uint32_t _query_end,
        std::vector<uint8_t> _seg_ids, std::vector<uint8_t> _strands):
    ref_name_1(_ref_name_1),
    ref_pos1(_ref_pos1),
    ref_name_2(_ref_name_2),
    ref_pos2(_ref_pos2),
    length(_length),
    evidence_type(_evidence_type),
    query_name(_query_name),
    query_start(_query_start),
    query_end(_query_end),
    seg_ids(_seg_ids),
    strands(_strands)
{
}

void Breakpoint::get_ins_seq(const bam1_t *b) {
    std::string query_seq;
    uint8_t *seq_prt = bam_get_seq(b);
    for (int32_t i = 0; i < b->core.l_qseq; ++i) {
        query_seq.push_back(seq_nt16_str[bam_seqi(seq_prt, i)]);
    }

    if (query_start <= query_seq.size() - 1
        && query_end <= query_seq.size() - 1 &&
        query_start < query_end)
    {
        sequence = query_seq.substr(query_start+1, query_end-query_start);
    } else {
        fprintf(stderr, "[Breakpoint::get_ins_seq] Error query start or "
            "query end out of range. Evidence: %d\n", (int)evidence_type);
        // need specify the evidence type
    }
}

std::vector<std::unique_ptr<Breakpoint>> get_breakpoint_s(
    const std::vector<std::unique_ptr<Segment>> &segments,
    const int min_sv_length)
{
    std::vector<std::unique_ptr<Breakpoint>> _breakpoints;
    // test adjacent segment evidence
    if (segments.size() >= 2) {
        for (uint32_t i = 0; i < segments.size() - 1; ++i) {
            uint32_t j = i + 1;
            std::unique_ptr<Breakpoint> _breakpoint = get_breakpoint_p(
                segments[i], segments[j], min_sv_length);
            if (_breakpoint != nullptr) {
                _breakpoints.push_back(std::move(_breakpoint));
            }
        }
    }
    return _breakpoints;
}

std::vector<std::unique_ptr<Breakpoint>> get_breakpoint_a(
    const bam_hdr_t *header, const bam1_t *b, const uint32_t min_sv_length)
{
    auto main_segment = get_Segment_main(header, b);
    if (!segment_filter(main_segment)) {
        return std::vector<std::unique_ptr<Breakpoint>>();
    }

    auto main_segment_id = get_Segment_main_id(header, b);

    // breakpoint init
    std::string _ref_name_1;
    uint32_t _ref_pos1;
    std::string _ref_name_2;
    uint32_t _ref_pos2;
    int _sv_length;
    Evidence_type _evidence_type = Evidence_type::SP;
    std::string _query_name;
    uint32_t _query_start;
    uint32_t _query_end;

    std::vector<std::unique_ptr<Breakpoint>> _breakpoints;

    std::string ref_name = header->target_name[b->core.tid];
    uint32_t current_ref_pos = b->core.pos;
    uint32_t current_query_pos;
    if (main_segment->strand == '+') {
        current_query_pos = 0;
    } else {
        current_query_pos = main_segment->query_length - 1;
    }

    uint32_t *ca_prt = bam_get_cigar(b);

    uint8_t c_op = 0;
    uint32_t c_oplen = 0;
    uint8_t c_type = 0;

    for (uint32_t i = 0; i < b->core.n_cigar; ++i) {
        c_op = bam_cigar_op(*(ca_prt + i));
        c_oplen = bam_cigar_oplen(*(ca_prt + i));
        c_type = bam_cigar_type(c_op);

        if (c_type & 2) {
            current_query_pos += c_oplen;
        }

        if (c_type & 1) {
            if (main_segment->strand == '+') {
                current_query_pos += c_oplen;
            } else {
                current_query_pos -= c_oplen;
            }
        }

        if (c_op == BAM_CINS && c_oplen >= min_sv_length) {
            std::vector<uint8_t> _seg_ids; // init
            std::vector<uint8_t> _strands; // init

            _ref_name_1 = ref_name;
            _ref_name_2 = ref_name;
            _ref_pos1 = current_ref_pos;
            _ref_pos2 = current_ref_pos;
            _sv_length = c_oplen;

            _query_name = main_segment->query_name;
            
            if (main_segment->strand == '+') {
                _query_start = current_query_pos - c_oplen + 1;
                _query_end = current_query_pos;
                _strands.push_back(1);
            } else {
                _query_start = current_query_pos;
                _query_end = current_query_pos + c_oplen - 1;
                _strands.push_back(0);
            }


            _seg_ids.push_back(main_segment_id);
            

            _breakpoints.push_back(std::unique_ptr<Breakpoint>(new Breakpoint(
                _ref_name_1, _ref_pos1, _ref_name_2, _ref_pos2, _sv_length,
                 _evidence_type, _query_name, _query_start, _query_end,
                 _seg_ids, _strands)));
        }
    }
    return _breakpoints;
}

std::unique_ptr<Breakpoint> get_breakpoint_p(
    const std::unique_ptr<Segment> &seg1, const std::unique_ptr<Segment> &seg2,
    const int min_sv_length)
{
    std::unique_ptr<Breakpoint> _breakpoint;
    if ((_breakpoint = get_breakpoint_p_ins(seg1, seg2, min_sv_length))
        != nullptr) {
        return _breakpoint;
    } else {
        return nullptr;
    }
}

void qpos_checker(const uint32_t &qStart, const uint32_t &qEnd,
    const uint32_t &qLen, const std::string &qName, const int &Line)
{
    if (qStart >= qEnd) {
        fprintf(stderr,
        "[qpos_checker] Error! %d, qStart > qEnd. %s\n", Line, qName.c_str());
    }
    if (qStart > qLen) {
        fprintf(stderr,
        "[qpos_checker] Error! %d, qStart > qLen. %s\n", Line, qName.c_str());
    }
    if (qEnd > qLen) {
        fprintf(stderr,
        "[qpos_checker] Error! %d, qEnd > qLen. %s\n", Line, qName.c_str());
    }
}

std::unique_ptr<Breakpoint> get_breakpoint_p_ins(
    const std::unique_ptr<Segment> &seg1, const std::unique_ptr<Segment> &seg2,
    const int min_sv_length)
{
    // init
    std::string _ref_name_1;
    uint32_t _ref_pos1;
    std::string _ref_name_2;
    uint32_t _ref_pos2;
    int _sv_length;
    Evidence_type _evidence_type = Evidence_type::SP;
    std::string _query_name;
    uint32_t _query_start;
    uint32_t _query_end;
    std::vector<uint8_t> _seg_ids;
    std::vector<uint8_t> _strands;

    // distance of two segment on query, may be negative
    int32_t dist_on_query = seg2->query_start - seg1->query_end;

    // distance of two segment on ref, may be negative
    int32_t dist_on_ref = 0;

    if (seg1->ref_name == seg2->ref_name) {
        if (seg1->strand == '+' && seg2->strand == '+') {
            dist_on_ref = seg2->ref_start - seg1->ref_end;
            if (dist_on_query > 0 &&
                dist_on_query - dist_on_ref >= min_sv_length)
            {
                _ref_name_1 = seg1->ref_name;
                _ref_name_2 = seg2->ref_name;
                
                _ref_pos1 = seg1->ref_end;
                _ref_pos2 = seg2->ref_start;

                _query_name = seg1->query_name;
                _query_start = seg1->query_end;
                _query_end = seg2->query_start;
                
                _sv_length = dist_on_query;

                _seg_ids.push_back(seg1->seg_id);
                _seg_ids.push_back(seg2->seg_id);

                _strands.push_back(seg1->strand == '+' ? 1 : 0);
                _strands.push_back(seg2->strand == '+' ? 1 : 0);

                return std::unique_ptr<Breakpoint>(new Breakpoint(_ref_name_1,
                    _ref_pos1, _ref_name_2, _ref_pos2, _sv_length,
                    _evidence_type, _query_name, _query_start, _query_end,
                    _seg_ids, _strands));
            }
        } else if (seg1->strand == '-' && seg2->strand == '-') {
            dist_on_ref = seg1->ref_start - seg2->ref_end;
            if (dist_on_query > 0 &&
                dist_on_query - dist_on_ref >= min_sv_length)
            {
                _ref_name_1 = seg1->ref_name;
                _ref_name_2 = seg1->ref_name;
                
                _ref_pos1 = seg2->ref_end;
                _ref_pos2 = seg1->ref_start;

                _query_name = seg1->query_name;
                _query_start = seg1->query_end;
                _query_end = seg2->query_start;

                _sv_length = dist_on_query;

                _seg_ids.push_back(seg1->seg_id);
                _seg_ids.push_back(seg2->seg_id);
                
                _strands.push_back(seg1->strand == '+' ? 1 : 0);
                _strands.push_back(seg2->strand == '+' ? 1 : 0);

                return std::unique_ptr<Breakpoint>(new Breakpoint(_ref_name_1,
                    _ref_pos1, _ref_name_2, _ref_pos2, _sv_length,
                    _evidence_type, _query_name, _query_start, _query_end,
                    _seg_ids, _strands));
            }
        }
    }
    return nullptr;
}