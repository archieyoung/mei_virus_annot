/*
 * author: Archieyoung<yangqi2@grandomics.com>
 */
#include "segments.h"
#include <algorithm>


Segment::~Segment() {
}

std::vector<std::unique_ptr<Segment>> get_Segments(const bam_hdr_t *header,
    const bam1_t *b, bool do_filter)
{   
    std::vector<std::unique_ptr<Segment>> _segments;
    uint8_t *SA_tag = bam_aux_get(b, "SA");
    
    if (SA_tag == nullptr) {
        return _segments;
    }

    std::string sa = bam_aux2Z(SA_tag);

    if (sa.empty()) {
        fprintf(stderr, "[segments::get_Segments] Error, empty SA field\n");
        std::exit(1);
    }

    _segments.push_back(get_Segment_main(header, b));

    std::string _query_name = bam_get_qname(b);
    uint32_t l_query = b->core.l_qseq;

    std::vector<std::unique_ptr<Segment>> _segments_sa = get_Segments_SA(
        sa, l_query, _query_name);
    for (auto &seg : _segments_sa) {
        _segments.push_back(std::move(seg));
    }
    std::sort(_segments.begin(), _segments.end(), segment_comp);

    // add id to segment, the id is its rank in the vector, begin at 0.
    for (std::vector<std::unique_ptr<Segment>>::size_type i = 0;
        i < _segments.size(); ++i)
    {
        _segments[i]->seg_id = i;
    }

    if (do_filter) {
            std::vector<std::unique_ptr<Segment>> _segments_keep;
            segments_filter(_segments, _segments_keep);
            return _segments_keep;
    } else {
        return _segments;
    }
}


std::unique_ptr<Segment> get_Segment_main(const bam_hdr_t *header,
    const bam1_t *b)
{
    const std::string _ref_name = header->target_name[b->core.tid];
    std::string _query_name = bam_get_qname(b);

    uint32_t *ca_prt = bam_get_cigar(b);
    uint32_t ca_len = b->core.n_cigar;
    uint32_t _pos = b->core.pos;
    uint32_t l_query = b->core.l_qseq;
    char _strand = get_strand(b->core.flag);

    // uint32_t _ref_start = pos;
    uint32_t _ref_end = get_ref_end(ca_prt, ca_len, _pos);
    
    uint32_t _query_start = get_query_start(ca_prt, _strand, l_query);
    uint32_t _query_end = get_query_end(ca_prt, ca_len, _strand, l_query);
    if (_query_start > _query_end) {
        std::swap(_query_start, _query_end);
    }

    uint8_t *NM_tag = bam_aux_get(b, "NM");
    if (NM_tag == nullptr) {
        fprintf(stderr,
            "[segments::get_Segment_main] Error! Can't find \"NM\" tag.\n");
        std::exit(-1);
    }
    int64_t nm = bam_aux2i(NM_tag);
    float _identity = gap_compressed_identity(nm, ca_prt, ca_len);

    return std::unique_ptr<Segment>(new Segment(_ref_name, _pos, _ref_end,
        _query_name, _query_start, _query_end, l_query, b->core.qual,
        _identity, _strand));
}

uint8_t get_Segment_main_id(const bam_hdr_t *header, const bam1_t *b) {
    auto main_segment = get_Segment_main(header, b);
    auto _segments = get_Segments(header, b, false);
    if (!_segments.empty()) {
        for (const auto &it: _segments) {
            if(main_segment->query_start == it->query_start &&
                main_segment->query_end == it->query_end)
            {
                return it->seg_id;
            }
        }
        // not find seg id
        fprintf(stderr, "[segments::get_Segment_main_id] Error! Can't find main"
            "record segment is segment vector.\n");
        std::exit(1);
    } else {
        // not a split mapping reads
        return 0;
    }
}

std::vector<std::unique_ptr<Segment>> get_Segments_SA(
    const std::string &sa_str, const uint32_t l_query,
    const std::string &_query_name)
{
    std::vector<std::unique_ptr<Segment>> _segments;
    // tokenize
    std::vector<std::string> sa_vec;
    Tokenize(sa_str, sa_vec, ';');
    for (const auto &record : sa_vec) {
        // SA fields: rname, pos, strand, CIGAR, mapQ, NM
        std::vector<std::string> sa_fields;
        Tokenize(record, sa_fields, ',');
        // uint32_t *ca_prt = new uint32_t[262144]; // tmp use need refine

        uint32_t ca_len;
        uint32_t *ca_prt = cigar_str_to_bcigar(sa_fields[3], ca_len);
        if (ca_prt == nullptr) {
            fprintf(stderr, "Get CIGAR from SA failed. "
                "Truncated or too many CIGAR operations\n");
            std::exit(1);
        }
        // minus 1 to '0-based' coordinate
        uint32_t _pos = std::stoi(sa_fields[1])-1; 
        int32_t nm = std::stoi(sa_fields[5]);
        char _strand = sa_fields[2][0];
        
        std::string _ref_name = sa_fields[0];
        uint32_t _ref_start = _pos;
        uint32_t _ref_end = get_ref_end(ca_prt, ca_len, _pos);
        
        uint32_t _query_start = get_query_start(ca_prt, _strand, l_query);
        uint32_t _query_end = get_query_end(ca_prt, ca_len, _strand, l_query);
                
        if (_query_start > _query_end) {
            std::swap(_query_start, _query_end);
        }
        uint32_t _query_length = l_query;

        // mapping identity
        float _identity = gap_compressed_identity(nm, ca_prt, ca_len);

        free(ca_prt);

        uint8_t _qual = std::stoi(sa_fields[4]);

        _segments.push_back(std::move(std::unique_ptr<Segment>(
            new Segment(sa_fields[0], _ref_start, _ref_end,
            _query_name, _query_start, _query_end,
            _query_length, _qual, _identity, _strand))));
    }
    return _segments;
}


uint32_t* cigar_str_to_bcigar(const std::string &cigar_str,
    uint32_t &n_cigar_op) {
    std::vector<uint32_t> cigar_vec;
    std::string op_len_str;
    uint32_t op_len;
    uint8_t op; //'MIDNSHP=XB' -> '0123456789'
    uint8_t is_op_set = 0; // for check if op is appear
    n_cigar_op = 0;
    for (const auto &c : cigar_str) {
        switch (c) {
            case 'M' : op = 0; is_op_set = 1; break;
            case 'I' : op = 1; is_op_set = 1; break;
            case 'D' : op = 2; is_op_set = 1; break;
            case 'N' : op = 3; is_op_set = 1; break;
            case 'S' : op = 4; is_op_set = 1; break;
            case 'H' : op = 5; is_op_set = 1; break;
            case 'P' : op = 6; is_op_set = 1; break;
            case '=' : op = 7; is_op_set = 1; break;
            case 'X' : op = 8; is_op_set = 1; break;
            case 'B' : op = 9; is_op_set = 1; break;
            default : op_len_str.push_back(c); break;
        }
        if (is_op_set) {
            if (!op_len_str.empty()) {
                op_len = std::stoi(op_len_str); // string to int 
                // *cigar_array = op_len << 4 | op;
                // ++cigar_array;
                cigar_vec.push_back(op_len << 4 | op);
                ++n_cigar_op;
                is_op_set = 0; // unset
                op_len_str.clear(); // clear
            } else {
                return nullptr; // operator cannot appear before operator length
            }
        }
    }
    if (n_cigar_op >= 1U<<29) return nullptr; // to many operations, 2^29
    uint32_t *ca_tmp = (uint32_t *)calloc(n_cigar_op, sizeof(uint32_t));
    std::copy(cigar_vec.begin(), cigar_vec.end(), ca_tmp);
    return ca_tmp;
}


uint32_t get_ref_span(const uint32_t *ca_prt, const uint32_t &ca_len)
{
    uint32_t ref_span = 0;
    // loop through cigar array
    for (uint32_t i = 0; i < ca_len; ++i) {
        const uint8_t c_op = bam_cigar_op(*(ca_prt+i));
        const int c_type = bam_cigar_type(c_op);
        // bit 2 means the cigar operation consumes the reference
        if (c_type&2) {
            ref_span += bam_cigar_oplen(*(ca_prt+i));
        }
    }
    return ref_span;
}


uint32_t get_ref_end(const uint32_t *ca_prt, const uint32_t &ca_len,
    const uint32_t &pos) {
    return pos+get_ref_span(ca_prt, ca_len)-1;
}


uint32_t get_query_start(const uint32_t *ca_prt, const char &strand,
    const uint32_t &l_query)
{
    const uint8_t c_op = bam_cigar_op(*ca_prt);
    const uint32_t c_oplen = bam_cigar_oplen(*ca_prt);
    
    // if the read is reversed
    if (strand == '-') {
        // is the start seq of the query clipped?
        if (c_op == BAM_CSOFT_CLIP || c_op == BAM_CHARD_CLIP) {        
            return l_query - 1 - c_oplen; // 0-based position
        } else {
            return l_query - 1; // 0-based position
        }
    } else {
        // is the start seq of the query clipped?
        if (c_op == BAM_CSOFT_CLIP || c_op == BAM_CHARD_CLIP) {
            return c_oplen; // 0-based position
        } else {
            return 0; // 0-based position
        }
    }
}


uint32_t get_query_end(const uint32_t *ca_prt, const uint32_t &ca_len, 
    const char &strand, const uint32_t &l_query) {
    const uint8_t c_op = bam_cigar_op(*(ca_prt+ca_len-1));
    const uint32_t c_oplen = bam_cigar_oplen(*(ca_prt+ca_len-1));
    
    // if the read is reversed
    if (strand == '-') {
        // is the end seq of the query clipped?
        if (c_op == BAM_CSOFT_CLIP || c_op == BAM_CHARD_CLIP) {        
            return c_oplen;
        } else {
            return 0;
        }
    } else {
        // is the end seq of the query clipped?
        if (c_op == BAM_CSOFT_CLIP || c_op == BAM_CHARD_CLIP) {
            return l_query - 1 - c_oplen;
        } else {
            return l_query - 1;
        }
    }
}


// compute gap compressed identity.
// https://lh3.github.io/2018/11/25/on-the-definition-of-sequence-identity
// identity = (NM - D - I + O)/(M + O)
// remove gap extend but keep gap open, when calculate identity.
// NM is NM tag in sam file, which represent not-match bases.
// O is number of gap open.
float gap_compressed_identity(const int32_t &nm, const uint32_t *ca_prt,
    const uint32_t &ca_len)
{
    uint32_t l_m = 0;
    uint32_t l_d = 0;
    uint32_t l_i = 0;
    uint32_t l_o = 0;
    for (uint32_t i = 0; i < ca_len; ++i) {
        const int c_op = bam_cigar_op(*(ca_prt+i));
        const int c_oplen = bam_cigar_oplen(*(ca_prt+i));
        if (c_op == BAM_CMATCH) {
            l_m += c_oplen;
        } else if (c_op == BAM_CDEL) {
            l_d += c_oplen;
            ++l_o;
        } else if (c_op == BAM_CINS) {
            l_i += c_oplen;
            ++l_o;
        }
    }
    float _identity = 1 - float(nm - l_d - l_i + l_o)/(l_m + l_o);
    return _identity;
}

void segments_filter(std::vector<std::unique_ptr<Segment>> &segments, 
    std::vector<std::unique_ptr<Segment>> &segments_keep)
{
    for (auto &seg: segments) {
        if (segment_filter1(seg)) {
            segments_keep.push_back(std::move(seg));
        }
    }
}

// segments part len ratio filter is too strict and could not handle segments
// near Ns well, so this method is deprecated
// void segments_filter(std::vector<std::unique_ptr<Segment>> &segments, 
//     std::vector<std::unique_ptr<Segment>> &segments_keep)
// {
//     auto segment_num = segments.size();
//     int part_len = 0;
//     uint32_t seg_len = 0;
//     float seg_len_rp = 0;

//     std::vector<int> segment_mark(segment_num, 1);
    
//     for (int i = 0; i < segment_num; ++i) {
//         if (!segment_filter1(segments[i])) {
//             segment_mark[i] = 0;
//         }
        
//         seg_len = segments[i]->query_end - segments[i]->query_start;
//         if (i == 0) {
//             part_len = segments[i]->query_end;
//             seg_len_rp = float(seg_len)/part_len;
//             if (seg_len_rp < 0.5) {
//                 segment_mark[i] = 0;
//             }
//         } else if (i == segment_num - 1) {
//             part_len = segments[i]->query_length - segments[i]->query_start;
//             seg_len_rp = float(seg_len)/part_len;
//             if (seg_len_rp < 0.5) {
//                 segment_mark[i] = 0;
//             }
//         } else {
//             int pre_seg_idx = i-1;
//             while(pre_seg_idx >= 0 && segment_mark[pre_seg_idx] == 0) {
//                 --pre_seg_idx;
//             }
//             if (pre_seg_idx >= 0) {
//                 part_len = (segments[i+1]->query_start -
//                     segments[pre_seg_idx]->query_end + 1);
//                 if (part_len <= 0) {
//                     fprintf(stderr, "[segments::segments_filter] "
//                     "Warning Part length <= 0.\n");
//                 }
//                 seg_len_rp = float(seg_len)/part_len;
//                 if (seg_len_rp < 0.5) {
//                     segment_mark[i] = 0;
//                 }
//             } else {
//                 part_len = segments[i]->query_end;
//                 seg_len_rp = float(seg_len)/part_len;
//                 if (seg_len_rp < 0.5) {
//                     segment_mark[i] = 0;
//                 }
//             }  
//         }
//     }
    
//     for (int i = 0; i < segment_num; ++i) {
//         if ( segment_mark[i] == 1) {
//             segments_keep.push_back(std::move(segments[i]));
//         }
//     }
// }