#ifndef SEGMENTS_H
#define SEGMENTS_H

#include <vector>
#include <string>
#include <iostream>
#include <memory>

#include "htslib/sam.h"
#include "yutils.h"


class Segment {
    public:
    Segment() = default;
    Segment(const std::string &_ref_name,
        const uint32_t &_ref_start,
        const uint32_t &_ref_end,
        const std::string &_query_name,
        const uint32_t &_query_start,
        const uint32_t &_query_end,
        const uint32_t &_query_length,
        const uint8_t &_mapping_qual,
        const float &_identity,
        const char &_strand): ref_name(_ref_name),
        ref_start(_ref_start),
        ref_end(_ref_end),
        query_name(_query_name),
        query_start(_query_start),
        query_end(_query_end),
        query_length(_query_length),
        mapping_qual(_mapping_qual),
        identity(_identity),
        strand(_strand) {};
    ~Segment();

    std::string ref_name;
    uint32_t ref_start;
    uint32_t ref_end;
    std::string query_name;
    uint32_t query_start;
    uint32_t query_end;
    uint32_t query_length;
    uint8_t mapping_qual;
    float identity;
    char strand;
    uint8_t seg_id = 0;
};

std::vector<std::unique_ptr<Segment>> get_Segments(
    const bam_hdr_t *header, const bam1_t *b, bool do_filter = true);

std::unique_ptr<Segment> get_Segment_main(const bam_hdr_t *header,
    const bam1_t *b);

uint8_t get_Segment_main_id(const bam_hdr_t *header, const bam1_t *b);

std::vector<std::unique_ptr<Segment>> get_Segments_SA(
    const std::string &sa_str, const uint32_t l_query,
    const std::string &_query_name);

inline
bool segment_comp(const std::unique_ptr<Segment> &a,
    const std::unique_ptr<Segment> &b) {
    return a->query_start < b->query_start;
}

inline
bool segment_filter1(const std::unique_ptr<Segment> &seg) {
    int seg_len = seg->query_end - seg->query_start + 1;
    if (seg->mapping_qual < 30 || seg->identity < 0.8 || seg_len < 100) {
        return false;
    } else {
        return true;
    }
}

inline
bool segment_filter(const std::unique_ptr<Segment> &seg) {
    if (segment_filter1(seg)) {
        // float seg_r = float(seg->query_end -
        //     seg->query_start)/seg->query_length;
        // if (seg_r >= 0.5) {
        //     return true;
        // } else {
        //     return false;
        // }
        return true;
    } else {
        return false;
    }
}

void segments_filter(std::vector<std::unique_ptr<Segment>> &segments, 
    std::vector<std::unique_ptr<Segment>> &segments_keep);

/*
convert cigar string to bam format cigar for convenience
parameters: cigar string, number of cigar operations
(32 bits to allow more ciger ops, new standard)
return: a pointer to dynamic cigar array(need free after use), nullprt if failed
*/
uint32_t* cigar_str_to_bcigar(const std::string &cigar_str,
    uint32_t &n_cigar_op);

/*
ca_prt: pointer to bam cigar array
ca_len: length of the cigar operations
pos: start position on ref
flag: bam flag
l_query: query length
*/
// uint32_t get_ref_start(const uint32_t &pos);
uint32_t get_ref_span(const uint32_t *ca_prt, const uint32_t &ca_len);

uint32_t get_ref_end(const uint32_t *ca_prt, const uint32_t &ca_len,
    const uint32_t &pos);

char get_strand(const uint16_t &flag);

uint32_t get_query_start(const uint32_t *ca_prt, const char &strand,
    const uint32_t &l_query);

uint32_t get_query_end(const uint32_t *ca_prt, const uint32_t &ca_len, 
    const char &strand, const uint32_t &l_query);

float gap_compressed_identity(const int32_t &nm, const uint32_t *ca_prt,
    const uint32_t &ca_len);

inline
char get_strand(const uint16_t &flag) {
    if (flag & BAM_FREVERSE) {
        return '-';
    } else {
        return '+';
    }
}

// SA is a tag with data of string(Z) type
// SA: Other canonical alignments in a chimeric alignment
// SA fields: rname, pos, strand, CIGAR, mapQ, NM
#endif // end SEGMENTS_H