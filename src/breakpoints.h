#ifndef BREAKPOINTS_H
#define BREAKPOINTS_H

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <memory>
#include <map>

#include "segments.h"

// evidence type
enum class Evidence_type:uint8_t {AL, SP};

class Breakpoint {
    public:
    Breakpoint() = default;
    Breakpoint(std::string _ref_name_1, uint32_t _ref_pos1,
        std::string _ref_name_2, uint32_t _ref_pos2, int _length,
        Evidence_type _evidence_type, std::string _query_name,
        uint32_t _query_start, uint32_t _query_end,
        std::vector<uint8_t> _seg_ids, std::vector<uint8_t> _strands);
    ~Breakpoint(){};
    
    std::string ref_name_1;
    uint32_t ref_pos1;
    std::string ref_name_2;
    uint32_t ref_pos2;
    int length = 0; // 0 means not length available
    Evidence_type evidence_type;

    std::string query_name;
    uint32_t query_start;
    uint32_t query_end;
    std::string sequence; // INS sequece
    void get_ins_seq(const bam1_t *b);

    std::vector<uint8_t> seg_ids;
    std::vector<uint8_t> strands; // 0: '-', 1: '+'
};

// get breakpoints from adjacent segment pairs
std::vector<std::unique_ptr<Breakpoint>> get_breakpoint_s(
    const std::vector<std::unique_ptr<Segment>> &segments,
    const int min_sv_length);

// get breakpoints from alignment CIGAR
std::vector<std::unique_ptr<Breakpoint>> get_breakpoint_a(
    const bam_hdr_t *header, const bam1_t *b, const uint32_t min_sv_length);

// get breakpoint from a pair of segments
std::unique_ptr<Breakpoint> get_breakpoint_p(
    const std::unique_ptr<Segment> &seg1, const std::unique_ptr<Segment> &seg2,
    const int min_sv_length);

std::unique_ptr<Breakpoint> get_breakpoint_p_ins(
    const std::unique_ptr<Segment> &seg1, const std::unique_ptr<Segment> &seg2,
    const int min_sv_length);

#endif // BREAKPOINTS_H